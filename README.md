# Swagger CodeGen

Builds docker image based on Swagger CodeGen v3 CLI, and adds python environment. 

* https://hub.docker.com/r/swaggerapi/swagger-codegen-cli-v3

Creates a _swagger-codegen_ wrapper around the java call too, available on the path. The image is intended for the manipulation of codegen output using beautifulsoup python scripts.

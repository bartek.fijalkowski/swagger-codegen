FROM swaggerapi/swagger-codegen-cli-v3:latest AS codegenclean
WORKDIR /opt/swagger-codegen-cli/
ENTRYPOINT []

# Add small shell script for swagger codegen
RUN echo -e '#!/bin/sh\njava -jar /opt/swagger-codegen-cli/swagger-codegen-cli.jar "$@"' > /usr/bin/swagger-codegen
RUN chmod +x /usr/bin/swagger-codegen

# Add python 3 environment for manipulation of codegen output
RUN apk add python3 py3-pip
# Install any required modules (and update pip)
RUN pip3 install --no-cache-dir --upgrade pip bs4 html5lib
# Make some useful symlinks that are expected to exist
RUN cd /usr/bin && \
    ln -s pydoc3 pydoc && \
    ln -s python3 python && \
    ln -s python3-config python-config && \
    cd /
